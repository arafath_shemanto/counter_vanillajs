let count = 0;
const btns = document.querySelectorAll(".btn");
const counter_number = document.querySelector(".counter_number");

btns.forEach(function (btn) {
  btn.addEventListener("click", function (e) {
    let unicClass = e.currentTarget.classList;
    if (unicClass.contains("increase")) {
      count++;
    } else if (unicClass.contains("decrease")) {
      if (count > 0) {
        count--;
      }
    } else {
      count = 0;
    }
    counter_number.textContent = count;
  });
});
